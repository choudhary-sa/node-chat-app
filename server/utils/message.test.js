const expect = require('expect');

var {generateMessage,generateLocationMessage} = require('./message');

describe('generateMessage',() => {

  it('should generate the correct message object',() =>{
    var message = {
      from:'abc',
      text:'text'
    }

    var result = generateMessage(message.from,message.text);
    expect(result.createdAt).toBeA('number');
    expect(result.from).toBe(message.from);
    expect(result.text).toBe(message.text);
    // expect(result).toInclude({
    //   from:message.from,
    //   text:message.text
    // });
  });

});

describe('generateLocationMessage', () => {
  it('should generate correct location object',() => {
    var latitude = 1;
    var longitude = 1;
    var from = 'saurabh';
    var url = 'https://www.google.com/maps?q=1,1';
    var result = generateLocationMessage(from,latitude,longitude);
    expect(result.createdAt).toBeA('number');
    expect(result.from).toBe(from);
    expect(result.url).toBe(url);
  });
});
