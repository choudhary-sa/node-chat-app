const expect = require('expect');

const {Users} = require('./users');


describe('Users' ,() => {
  var users;
  beforeEach(() => {
    users = new Users();
    users.users = [{
      id:'1',
      name: 'Saurabh',
      room:'Home'
    },
    {
      id:'2',
      name: 'Jen',
      room:'Office'
    },
    {
      id:'3',
      name: 'Paul',
      room:'Office'
    }
  ];
  });

  it('should add new user',() => {
    var users = new Users();
    var user = {
      id:123,
      name:'Saurabh',
      room:'Home'
    }
    var res = users.addUser(user.id,user.name,user.room);
    expect(users.users).toEqual([user]);
  });

  it('should return names for Office',() => {
    var userList = users.getUserList('Office');
    expect(userList).toEqual(['Jen','Paul']);
  });

  it('should return names for Home',() => {
    var userList = users.getUserList('Home');
    expect(userList).toEqual(['Saurabh']);
  });

  it('should find a user',() => {
    var user = users.getUser('1');
    expect(user).toBe(users.users['0']);
  });

  it('should not find a user',() => {
    var user = users.getUser('4');
    expect(user).toNotExist();
  });


  it('should remove a user',() => {
    var user = users.users[1];
    var result = users.removeUser('2');
    expect(result).toBe(user);
  });


  it('should not remove a user',() => {
    var result = users.removeUser('4');
    expect(result).toNotExist();
  });




});
